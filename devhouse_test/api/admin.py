from django.contrib import admin


from .models import Shop, Schedule, TimeInterval
# Register your models here.


admin.site.register(Shop)
admin.site.register(Schedule)
admin.site.register(TimeInterval)
