from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from datetime import time
from datetime import datetime, date
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

class Shop(models.Model):
    """Shop model class."""
    name = models.CharField(max_length=250)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    is_open = models.BooleanField(default=True)
    def __str__(self):
        return self.name

    def getDay(self, code):
        """Return day by code."""
        try:
            day = self.schedule.get(day=code)
        except ObjectDoesNotExist as e:
            day = Schedule(day=code, shop=self)
        return day

    def createDefaultSchedule(self, fillEmpty=False):
        """Create default schedule for new shop."""
        weekend = ['sun', 'sat']
        if fillEmpty:
            days = dict(Schedule.DAYS).keys() - \
                        list(self.schedule.all().values_list('day', flat=True))
        else:
            days = dict(Schedule.DAYS).keys()

        for day in days:
            schedule = self.schedule.create(
                is_open=(day not in weekend),
                day=day,
            )
            if schedule.is_open:
                schedule.setDefShedule()
    @property
    def now_open(self):
        if not self.is_open:
            return False
        now = datetime.now()
        day = self.schedule.get(
            day=now.strftime("%a").lower()
        )
        intervals = self.schedule.filter(
            intervals__in=day.intervals.filter(
                is_open_time=True, end__gte=now.time(), start__lte=now.time()
            )
        ).exclude(
            intervals__in=day.intervals.filter(
                is_open_time=False, end__gte=now.time(), start__lte=now.time()
            )
        ).count()
        return intervals > 0

    @now_open.setter
    def now_open(self, value):
        self.is_open = value

    @property
    def schedule(self):
        return self._schedule

    @property
    def checkEmpty(self):
        return self._schedule.count() < 7

    @schedule.setter
    def schedule(self, data):
        """Update shop schedule."""
        if isinstance(data, dict):
            for day, value in data.items():
                schedule = self.getDay(day)
                schedule.updateTimes(value)
            if self.checkEmpty:
                self.createDefaultSchedule(self.checkEmpty)
            return
        if isinstance(data, bool):
            for day, val in dict(Schedule.DAYS).items():
                schedule = self.getDay(day)
                schedule.updateTimes(data)
            return
        self.createDefaultSchedule()


class Schedule(models.Model):
    """Schedule model class."""
    DAYS = (
        ('sun', 'Sunday'),
        ('mon', 'Monday'),
        ('tue', 'Tuesday'),
        ('wed', 'Wednesday'),
        ('thu', 'Thursday'),
        ('fri', 'Friday'),
        ('sat', 'Saturday'),
    )
    is_open = models.BooleanField(default=False)
    day = models.CharField(max_length=3, choices=DAYS)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE,
                             related_name="_schedule")

    def setDefShedule(self, start=8, end=19):
        self.is_open = True
        self.save()

        start = self.parseTime(start)
        end = self.parseTime(end)

        self.intervals.bulk_create([
            TimeInterval(start=start, end=end,
                         schedule=self),
            TimeInterval(start=time(12), end=time(13),
                         schedule=self, is_open_time=False,
                         type=2),
        ])

    def getInterval(self, type):
        try:
            if type > 1:
                raise ObjectDoesNotExist()
            interval = self.intervals.get(type=type)
        except ObjectDoesNotExist as e:
            interval = TimeInterval(schedule=self, type=type)
        return interval

    def setTime(self, start=8, end=19, reason='', type=1):
        start = self.parseTime(start)
        end = self.parseTime(end)
        interval = self.getInterval(type=type)

        interval.start = start
        interval.end = end
        interval.other_reason = reason
        interval.save()

    def parseTime(self, timeVal):
        if isinstance(timeVal, int):
            return time(timeVal)

        if isinstance(timeVal, str):
            return datetime.strptime(timeVal, '%H:%M').time()

        return timeVal

    def setTimeFromDict(self, data):
        self.intervals.all().delete()

        self.is_open = True
        self.save()
        types = dict(zip(dict(self.intervals.model.TYPES).values(),
                         dict(self.intervals.model.TYPES).keys()))

        for type_name, val in data.items():
            if isinstance(val[0], list):
                for multival in val:
                    multival.append(types[type_name])
                    self.setTime(*multival)
            else:
                val.append('')
                val.append(types[type_name])

                self.setTime(*val)

        self.save()

    def updateTimes(self, data):
        if isinstance(data, dict):
            self.setTimeFromDict(data)

        if isinstance(data, str):
            self.is_open = True
            self.setTime(start=data, type=1)
            self.save()

        if isinstance(data, list):
            self.is_open = True
            self.setTime(data[0], data[1])
            self.save()

        if isinstance(data, bool):
            if data:
                if self.is_open is False:
                    self.setDefShedule()
                else:
                    self.setTime()
            else:
                self.is_open = False
                self.intervals.all().delete()
            self.save()

    def getTimes(self):
        """Get times data for current schedule day."""
        if not self.is_open:
            return 'Free day'
        returnData = {}
        for interval in self.intervals.all():
            IType = interval.get_type_display()
            current = returnData.get(IType, False)
            if current:
                if isinstance(current, list):
                    current.append(interval.getStrInterval())
                else:
                    current = [current, interval.getStrInterval()]
                returnData.update({IType: current})
            else:
                returnData.update({IType: interval.getStrInterval()})

        return returnData

    def __str__(self):
        return f"{self.shop.name} - {self.day}"


class TimeInterval(models.Model):
    """Time model class."""

    TYPES = (
        (1, 'open'),
        (2, 'break'),
        (3, 'other'),
    )

    start = models.TimeField()
    end = models.TimeField()

    type = models.PositiveSmallIntegerField(default=1, choices=TYPES)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE,
                                 related_name="intervals")
    is_open_time = models.BooleanField(default=True)
    other_reason = models.CharField(max_length=250, default='', blank=True)

    def getStrInterval(self):
        """Get time interval as string value."""
        data = f"{self.start.strftime('%H:%M')} - {self.end.strftime('%H:%M')}"
        if self.other_reason:
            data+=f" {self.other_reason}"
        return data

    def __str__(self):
        return f"{self.schedule.day} - {self.getStrInterval()} / {self.get_type_display()}"
