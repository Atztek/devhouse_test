from rest_framework import permissions
from rest_framework.generics import (CreateAPIView, ListAPIView, UpdateAPIView,
                                     DestroyAPIView)
from django.contrib.auth.models import User
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from .serializers import UserSerializer, ShopSerializer
from .models import Shop
from rest_framework.parsers import JSONParser
from rest_framework import mixins
from rest_framework import generics
from django.db.models.query import QuerySet
from rest_framework.decorators import list_route
from rest_framework import status

class CreateUserView(CreateAPIView):
    model = User
    permission_classes = (permissions.AllowAny, )

    serializer_class = UserSerializer


class ShopView(ListAPIView):
    """Shop view class."""

    mode = Shop
    permission_classes = (permissions.AllowAny, )

    queryset = Shop.objects.all()
    serializer_class = ShopSerializer


class BaseShopView():
    model = Shop
    permission_classes = (permissions.IsAuthenticated,)

    parser_classes = (JSONParser,)

    serializer_class = ShopSerializer
    queryset = Shop.objects.all()

    def get_queryset(self):
        queryset = self.queryset
        if isinstance(queryset, QuerySet):
            queryset = queryset.filter(owner=self.request.user)
        return queryset

class ShopManageUView(BaseShopView, UpdateAPIView, DestroyAPIView):
    """Shop manage update view class."""
    def patch(self, request, *args, **kwargs):
        """Update shop."""
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, pk, format=None):
        snippet = self.get_object()
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ShopManageCLView(BaseShopView, CreateAPIView, ListAPIView):
    """Shop manage create/list view class."""

    def post(self, request, *args, **kwargs):
        """Create new shop."""
        request.data.update({'owner': request.user.pk})
        return self.create(request, *args, **kwargs)
