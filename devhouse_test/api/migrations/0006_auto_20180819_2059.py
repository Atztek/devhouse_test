# Generated by Django 2.1 on 2018-08-19 17:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_auto_20180815_0943'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='sis_open',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='timeinterval',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(1, 'open'), (2, 'break'), (3, 'other')], default=1),
        ),
    ]
