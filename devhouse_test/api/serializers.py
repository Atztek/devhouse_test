from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Shop, Schedule
from rest_framework.utils import model_meta
import inspect
from rest_framework.fields import empty

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    def create(self, validated_data):
        """Create user."""
        user = User.objects.create(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'first_name', 'last_name']

class IsOpenField(serializers.Field):
    def to_representation(self, obj):
        return obj

    def to_internal_value(self, data):
        return data

class ScheduleSerializer(serializers.ModelSerializer):
    def to_representation(self, obj):
        """Create format schedule."""
        list = {}
        for schedule in obj.all():
            list.update({schedule.day: schedule.getTimes()})
        return list

    def to_internal_value(self, data):
        return data

    def run_validation(self, data=empty):
        (is_empty_value, data) = self.validate_empty_values(data)
        if is_empty_value:
            return data

        value = self.to_internal_value(data)
        return value

    class Meta:
        model = Schedule
        fields = '__all__'

class ShopSerializer(serializers.ModelSerializer):
    schedule = ScheduleSerializer(required=False)
    now_open = IsOpenField(required=False)
    def create(self, validated_data):
        """Create shop."""
        shop = Shop.objects.create(
            name=validated_data['name'],
            owner=validated_data['owner'],
        )
        shop.schedule=validated_data['schedule']
        shop.save()
        return shop

    def update(self, instance, validated_data):
        #serializers.raise_errors_on_nested_writes('update', self, validated_data)
        info = model_meta.get_field_info(instance)
        # Simply set each attribute on the instance, and then save it.
        # Note that unlike `.create()` we don't need to treat many-to-many
        # relationships as being a special case. During updates we already
        # have an instance pk for the relationships to be associated with.
        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                field = getattr(instance, attr)

                field.set(value)
            else:
                setattr(instance, attr, value)
        instance.save()

        return instance

    class Meta:
        model = Shop
        fields = ('now_open', 'schedule', 'name', 'owner', 'id')
