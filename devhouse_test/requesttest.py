import requests
import json

class req():
    headers = {
        'Accept': 'application/json',
        'Content-Type': "application/json"
    }
    def get(self, url, token=False):
        headers = self.headers
        if token:
            headers.update({'Authorization': f'Token {token}'})

        r = requests.get(f"http://127.0.0.1:8000/api/{url}/", headers=headers)
        jsonData = json.loads(r.text)
        return jsonData

    def post(self, url, data, token=False):
        headers = self.headers
        if token:
            headers.update({'Authorization': f'Token {token}'})

        r = requests.post(f"http://127.0.0.1:8000/api/{url}/",
                          data=json.dumps(data), headers=headers)
        jsonData = json.loads(r.text)
        return jsonData

    def patch(self, url, data, token=False):
        headers = self.headers
        if token:
            headers.update({'Authorization': f'Token {token}'})

        r = requests.patch(f"http://127.0.0.1:8000/api/{url}/",
                          data=json.dumps(data), headers=headers)

        jsonData = json.loads(r.text)
        return jsonData

    def delete(self, url, token=False):
        headers = self.headers
        if token:
            headers.update({'Authorization': f'Token {token}'})

        r = requests.delete(f"http://127.0.0.1:8000/api/{url}/", headers=headers)

        jsonData = json.loads(r.text)
        return jsonData


request = req()
# create user
"""
post('user',{
    'username': 'someone',
    'password': 'test'
})
"""

# autentificate

"""
post('login',{
    'username': 'someone',
    'password': 'test'
})
"""

resp = request.post('login',{
    'username': 'someone',
    'password': 'test'
})
token = resp['token']

# create shop
"""
r = post("manage-shop", data={
    'name': 'KFC',
    'schedule': {
        'thu': {
            'open': ['10:00','21:00'],
            'break': ['13:00', '14:00'],
            'other': [
                ['14:00', '15:00', "break 2"],
                ['15:00', '16:00', "break 3"],
            ]
        }
    }
}, token=token)
"""




# get shop list
"""
r = requests.get("http://127.0.0.1:8000/api/shop/")
"""


r = request.delete("manage-shop/7", token=token)
print(json.dumps(r, indent=4))

"""
data = json.dumps({
    'name': 'Burgers',
    'now_open': False,
    'schedule': {
        'sun': ['10:00','20:00'],
        'mon': {
            'open': ['10:00','21:00'],
            'break': ['13:00', '14:00']
        },
        'sat': True,
        'thu': False,
}})

r = requests.patch("http://127.0.0.1:8000/api/manage-shop/10", data=data
, headers=headers)

print(r.status_code, r.reason)
print(r.text)
jsonData = json.loads(r.text)
print(json.dumps(jsonData, indent=4))
"""
